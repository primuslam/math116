from math import *
from fractions import *
import random


def main(num = 100):
    p, q = 1619, 12227
    N = p*q
    phi = (p-1)*(q-1)

    # possible = list(range(3, N))
    # random.shuffle(possible)
    for _ in range(num):
        try:
            e = random.choice(range(3, N))
            d = modinv(e, phi)
            break
        except ValueError:
            pass

    print("e = {}, d = {}, N = {}".format(e, d, N, num=num))

    print(factor(N, e, d))

def egcd(a, b):
    '''
    Extended Euclidean Greatest Common Divisor as provided in the textbook
    Return the GCD of a and b
    '''
    if a is 0:
        return b, 0, 1
    else:
        g, y, x = egcd(b % a, a)
        return g, x - (b // a) * y, y

def modinv(a, m):
    '''
    Find the modular inverse of a mod m, if it exists
    '''
    g, x, y = egcd(a, m)
    if g is not 1:
        raise ValueError('A is not coprime to M!')
    else:
        return x % m
    
def modexp(g, p, N):
    '''
    Fast modular power algorithm
    Find g**p%N
    '''
    output = 1
    while p:
        if p & 1:
            output = (output * g) % N
        g = g ** 2 % N
        p = p//2
    return output
    
def factor(N, e, d, num=100):
    possible = [x for x in range(2, min(N, num)) if gcd(x, N) == 1]
    #random.shuffle(possible)

    for a in possible:
        c = modexp(a, e, N)
        print("trying a = {}, a^e = {}, a^ed = {}".format(a, c, modexp(c, d, N)))

        x = 1
        s = (e*d-1)
        x_squared = 1
        while (s%2 == 0) and (x == 1 or x == N-1):
            s = s//2
            x_squared = x
            x = modexp(a, s, N)
            print("\ta^{} = x = {}, x^2 = {}".format(s, x, x_squared))
        if x != 1 and x != N-1:
            print("Excellent! We found a nontrivial square root {}.".format(x))
            p, q = gcd(x-1, N), gcd(x+1, N)
            p, q = max(p, q), min(p, q)
            return p, q
    #Need to try more a but ran out of time
    return None
            
main()
